module ApplicationHelper
  def self.date_convert(str_date)
    begin
      dd=str_date.to_time
      zone=ActiveSupport::TimeZone.new(BEIJING)
      return dd.in_time_zone(zone)
    rescue => e
      return nil
    end
  end

  def self.date_minus(d1,d2)
    return ((d1 - d2)/60).to_i #minutes
  end
  
  def active_class(link_path)
    current_page?(link_path) ? "nav-item active" : "nav-item"
  end

end
