module MatchJob
  class << self
    def job_logger
      @@job_logger||= Logger.new("#{Rails.root}/log/match_job#{Date.today.strftime('%Y%m%d')}.log")
    end
    def run
      begin
        job_logger.info ("MatchJob#run start #{Time.now}")
        room=Room.where.not(status: ROOM_FINISHED).first
        if room.present?
          rps=RoomPlayer.where(room_id: room.id, status: PLAYER_PLAYING)
          .joins("left join players on players.id = room_players.player_id")
          .select("room_players.*, players.name").limit(10)
          if rps.present?
            rps.each do |p|
              if ApplicationHelper.date_minus(Time.now, p.created_at) > TIME_LIMIT
                p.status=PLAYER_FINISHED
                p.save!
              else 
                #p.last_match="9a99b63d-ecc3-426f-9e84-673a7c086827"#Pubg.next_match(p.name, p.last_match)
                p.last_match=Pubg.next_match(p.name, p.last_match)
                if p.last_match.present?
                  p.save!
                  player=Player.find(p.player_id)
                  result=Pubg.matches(p.last_match, player.account)
                  if result[:status]==200
                    match=Match.new
                    match.room_id=p.room_id
                    match.player_id=player.id
                    match.match=p.last_match
                    match.damage_dealt=result[:damage_dealt]
                    match.kills=result[:kills]
                    match.win_place=result[:win_place]
                    if match.save
                      p.match_cnt=p.match_cnt+1
                      if p.match_cnt>=room.match_cnt
                        p.status=PLAYER_FINISHED
                      end  
                      p.save!
                    end
                  end
                end #end last match present
              end #end date_minus 
            end#end room player loop
          end
        else
          job_logger.info ("MatchJob#run room not found #{Time.now}")
        end
        job_logger.info ("MatchJob#run end #{Time.now}")
      rescue => e
        job_logger.error ("MatchJob#run #{e.to_s} #{Time.now}")
      end
    end
  end
end
