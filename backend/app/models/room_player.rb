class RoomPlayer < ApplicationRecord
  validates :player_id, uniqueness: { scope: :room_id }
end
