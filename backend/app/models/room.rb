class Room < ApplicationRecord
    def calc 
       if self.method==METHOD_NORMAL
        return self.calc_normal()
       elsif self.method==WINORLOSE
        return self.calc_winorlose()
       end
       return nil
    end   

    def calc_normal()
        Match.select('players.id, players.name, SUM((kills*16)+ppoints.point) as cp')
        .joins('left join players on matches.player_id=players.id')
        .joins('left join ppoints on ppoints.id=matches.win_place')
        .where(room_id: self.id).group('player_id')
        .order('cp desc')
    end
    
    def self.calc_winorlose()
  
    end 
end
