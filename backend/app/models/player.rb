class Player < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :validatable, :trackable, :omniauthable, omniauth_providers: [:steam]
  validates :name, uniqueness: true
  validates :account, uniqueness: true


  def self.from_omniauth(auth)
    where(uid: auth.uid).first_or_create do |player|
      player.uid = auth.uid
      player.name = auth.info.name
      player.nickname = auth.info.nickname
      player.location = auth.info.location
      player.image = auth.info.image
      player.password = Devise.friendly_token[0, 20]
    end
  end
end