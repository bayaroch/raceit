class Match < ApplicationRecord
  validates :player_id, uniqueness: { scope: :match }
end
