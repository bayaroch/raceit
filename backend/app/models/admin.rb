class Admin < ApplicationRecord
  devise :database_authenticatable, :registerable,
   :validatable, :omniauthable, omniauth_providers: [:google_oauth2]
    def self.find_from_auth_hash(auth)
        
        admin=where(provider: auth.provider, email: auth.info.email).first
        if admin.present?
            admin.provider = auth.provider
            admin.uid = auth.uid
            admin.name = auth.info.first_name
            admin.email = auth.info.email
            admin.save!
        end
        return admin
    end
end  