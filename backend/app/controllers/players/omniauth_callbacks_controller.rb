class Players::OmniauthCallbacksController < Devise::OmniauthCallbacksController
    skip_before_action :verify_authenticity_token, only: [:steam, :failure]
    def steam
        @player = Player.from_omniauth(request.env["omniauth.auth"])
        @player.save(:validate => false)
        if @player.present?
            sign_in_and_redirect @player, event: :authentication
        else
            
        end
    end

    def failure
        redirect_to root_path
    end
end