class Backend::DashboardController < ActionController::Base
    layout 'backend'
    before_action :authenticate_admin!

    def logout
        sign_out(current_admin)
        redirect_to backend_sessions_show_path
    end  
end