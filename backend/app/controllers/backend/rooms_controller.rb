class Backend::RoomsController < Backend::DashboardController
    before_action :set_model, only: [:show, :edit, :update]
    def index
        @rooms=Room.all
    end

    def new
        @room=Room.new
    end
    
    def create
        @room = Room.new(room_params)
        respond_to do |format|
            if @room.save
                format.html { redirect_to backend_root_path, notice: 'Room was successfully created.' }
            else
                format.html { render :new }
            end
        end
    end

    def show 
    end

    def edit
    end
    
    def update
        respond_to do |format|
            if @room.update(room_params)
                format.html { redirect_to backend_root_path, notice: 'Room was successfully updated.' }
                format.json { render :show, status: :ok, location: @room }
            else
                format.html { render :edit }
                format.json { render json: @room.errors, status: :unprocessable_entity }
            end
        end
    end
    
    private
        def set_model
            if(params.has_key?(:id))
                @room=Room.find(params[:id])
            else     
                @room=Room.find(params[:room][:id])
            end
        end

        def room_params
            params.fetch(:room).permit(:id, :name, :full_cnt, :match_cnt, :status, :method, :place_cnt, :ticket, :time_limit)
        end
end