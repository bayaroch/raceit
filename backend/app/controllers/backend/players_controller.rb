class Backend::PlayersController < Backend::DashboardController
    before_action :set_model, only: [:show]
    def index
        @players=Player.all
    end

    def show
        @rooms=RoomPlayer.where(player_id: @player.id)
          .joins("left join rooms on rooms.id = room_players.room_id")
          .select("room_players.*, rooms.name")
    end

    private
        def set_model
            @player=Player.find(params[:id])
        end
end
