class Backend::SessionsController < ApplicationController
  before_action :checkuser, only: [:show]
  layout nil
  def show

  end

  def checkuser
    if current_admin.present?
      redirect_to backend_root_path
    end
  end

end
