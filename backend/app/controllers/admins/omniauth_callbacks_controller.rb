class Admins::OmniauthCallbacksController < Devise::OmniauthCallbacksController
    skip_before_action :verify_authenticity_token, only: [:google_oauth2, :failure]
    def google_oauth2
        @admin = Admin.find_from_auth_hash(request.env["omniauth.auth"])
        if @admin.present?
            sign_in_and_redirect @admin, event: :authentication
        else 
            redirect_to backend_sessions_show_path
        end
    end

    def failure
        redirect_to backend_sessions_show_path
    end

    def after_sign_in_path_for(resource)
        stored_location_for(resource) || backend_root_path
    end
end