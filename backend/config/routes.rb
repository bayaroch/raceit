Rails.application.routes.draw do
  devise_for :players, :controllers => { :omniauth_callbacks => "players/omniauth_callbacks" }, :skip => [:sessions, :registration]
  devise_for :admins, :controllers => { :omniauth_callbacks => "admins/omniauth_callbacks" }, :skip => [:sessions, :registration]
  devise_scope :admins do
    get 'logout', to: 'backend/dashboard#logout', as: :destroy_admin_session
  end

  root :to => "home#index"
  get  'login', to: "home#login"
  get  'join', to: "home#join"
  get  'profile', to: "frontend/profile#index"
  
  namespace :backend do
    root :to => "rooms#index"
    resource :rooms
    resource :players do
      collection do
        get  'list', to: "players#index"
      end
    end
    get '/sessions/show', to: 'sessions#show', as: 'sessions_show'
  end  
end
