# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
PUBG_URL='https://api.pubg.com/shards/steam/'

INVOICE_DATETIME_FORMAT='%Y-%m-%d %H:%M:%S.012'
SIMPLE_DATETIME_FORMAT='%Y-%m-%d %H:%M:%S'
MATCH_CNT=3
TIME_LIMIT=120 #minutes
BEIJING='Beijing'
GAME_SOLO='solo'
GAME_SOLO_FPP='solo-fpp' #-->
GAME_SQUAD='squad'
GAME_SQUAD_FPP='squad-fpp'
MATCHES='matches'
PARTICIPANT='participant'
ATTRIBUTES='attributes'
STATS='stats'
DATA='data'
ID='id'
RELATIONSHIPS='relationships'
TYPE='type'
GAME_MODE='gameMode'
INCLUDED='included'
KILLS='kills'
WIN_PLACE='winPlace'
PLAYER_ID='playerId'
DAMAGE_DEALT='damageDealt'
IS_CUSTOM_MATCH='isCustomMatch'
CREATED_AT='createdAt'
ROOM_OPEN=0
ROOM_CLOSED=1
ROOM_FINISHED=2
PLAYER_PLAYING=0
PLAYER_FINISHED=1
METHOD_NORMAL=0
METHOD_SIMPLE=1
WINORLOSE=1

#JOBS
JOB_MATCH="match"

#PAYMENT METHODS
PAY_Q=1
PAY_CANDY=2
PAY_SOCIAL=3