Devise.setup do |config|  
    config.navigational_formats = []
    require 'devise/orm/active_record'
    config.authentication_keys = [:uid]
    config.case_insensitive_keys = [:uid]
    config.strip_whitespace_keys = [:uid]
    config.omniauth :steam, ENV['STEAM_API_KEY']
    config.omniauth :google_oauth2, ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'], scope: 'email,profile'
    config.omniauth_path_prefix = '/social/auth'
    # passing skip: :sessions to `devise_for` in your config/routes.rb
    config.skip_session_storage = [:http_auth]
    config.stretches = Rails.env.test? ? 1 : 11
    # Set up a pepper to generate the hashed password.
    config.expire_all_remember_me_on_sign_out = true
    config.sign_out_via = :get
    config.password_length = 6..128
end
  