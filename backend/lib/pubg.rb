module Pubg
  class << self

    def lib_logger
      @@lib_logger||= Logger.new("#{Rails.root}/log/pubg#{Date.today.strftime('%Y%m%d')}.log")
    end

    def headers
      headers = {
        "Authorization"  => ENV['PUBG_API_KEY'],
        "Accept" => "application/vnd.api+json"
      }
      return headers
    end

    def last_match(player)
      begin
        result=Hash.new
        res=HTTParty.get(
          "#{PUBG_URL}players?filter[playerNames]=#{player}",
          :headers => headers
        )
        if res.code==200
          obj = JSON.parse(res.body)
          data = obj[DATA][0]
          result[:account]=data[ID]
          matches=data[RELATIONSHIPS][MATCHES][DATA]
          last=matches.first
          if last.present?
            result[:match]=last[ID]
            return result
          end
        end
      rescue => e
        lib_logger.error("Pubg::last_match #{e.to_s} :: #{Time.now}")
      end
      return nil
    end

    def next_match(player, last_match)
      next_match_id=nil
      begin
        res=HTTParty.get(
          "#{PUBG_URL}players?filter[playerNames]=#{player}",
          :headers => headers
        )
        obj = JSON.parse(res.body)
        puts "-------1111111"
        puts res.body
        puts "-------2222222"
        data = obj[DATA][0]
        matches=data[RELATIONSHIPS][MATCHES][DATA]
        ids=[]
        matches.each do |m|
          id=m["id"]
          if  id!=last_match
            next_match_id=id
          elsif id==last_match
            break
          end
        end
      rescue => e
        lib_logger.error("Pubg::next_match #{e.to_s} :: #{Time.now}")
      end
      return next_match_id
    end

    def player(player)
      res=HTTParty.get(
        "#{PUBG_URL}players?filter[playerNames]=#{player}",
        :headers => headers
      )
      obj = JSON.parse(res.body)
      data = obj[DATA][0]
      puts "-------------data"
      puts obj
      puts "-------------data"
      matches=data[RELATIONSHIPS][MATCHES][DATA]
      last="59b96345-8837-4fdc-b46f-2b4e477a80e1"
      ids=[]
      puts "-------------data"
      matches.each do |m|
        id=m["id"]
        puts "--->#{id}"
        if  id!=last
          ids.prepend(id) #add element at the end
        elsif id==last
          break
        end
      end
      puts "---->#{ids}"
      #puts "---->#{ordered}"
      puts "-------------data"
      # puts "-------------data"
      # #puts matches
      # puts "-------------data"
    end

    def matches(id,playerId)
      result=Hash.new
      result[:status]=400
      res=HTTParty.get(
        "#{PUBG_URL}matches/#{id}",
        :headers => headers
      )
      if res.code==200
        obj = JSON.parse(res.body)
        data = obj[DATA]
        if data[ATTRIBUTES][IS_CUSTOM_MATCH]==false and data[ATTRIBUTES][GAME_MODE]==GAME_SOLO_FPP
          result[:created_at]=data[ATTRIBUTES][CREATED_AT]
          included=obj[INCLUDED]
          included.each do |i|
            if i[TYPE]==PARTICIPANT and i[ATTRIBUTES][STATS][PLAYER_ID]==playerId
              result[:status]=200
              result[:damage_dealt] = i[ATTRIBUTES][STATS][DAMAGE_DEALT]
              result[:kills] = i[ATTRIBUTES][STATS][KILLS]
              result[:win_place] = i[ATTRIBUTES][STATS][WIN_PLACE]
              break
            end
          end
        end
      end
      return result
    end #end matches

  end
end
