class AddDeviseToAdmins < ActiveRecord::Migration[5.2]
  def self.up
    create_table :admins do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :provider, :limit => 50, null: false
      t.string :uid, :limit => 50, null: false, :unique => true
      t.string :name, :limit => 50, null: false
      t.string :encrypted_password, null: false, default: ""
      t.integer :role, null: false, :default => 0
      
      t.timestamps null: false
    end

    add_index :admins, :email,                unique: true
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
