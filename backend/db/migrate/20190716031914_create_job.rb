class CreateJob < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :name, :limit => 50, null: false
      t.integer :status, null: false, :default => 0
      t.timestamps
    end
  end
end
