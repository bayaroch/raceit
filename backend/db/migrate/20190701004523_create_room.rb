class CreateRoom < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.string :name, :limit => 100, null: false
      t.integer :full_cnt, null: false, :default => 50
      t.integer :member_cnt, null: false, :default => 0
      t.integer :match_cnt, null: false, :default => 3
      t.integer :status, null: false, :default => 0
      t.integer :method, null: false, :default => 0
      t.integer :place_cnt, null: false, :default => 10
      t.integer :ticket, null: false, :default => 1000
      t.decimal :amount, null: false, :default => 0
      t.integer :time_limit, null: false, :default => 120
      t.timestamps
    end
  end
end
