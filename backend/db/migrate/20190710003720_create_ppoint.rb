class CreatePpoint < ActiveRecord::Migration[5.2]
  def change
    create_table :ppoints do |t|
      t.integer :point, null: false, :default => 0
    end
  end
end
