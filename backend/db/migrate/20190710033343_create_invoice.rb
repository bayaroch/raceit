class CreateInvoice < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.references :player, index: true, foreign_key: true
      t.decimal :amount, null: false #invoice amount
      t.integer :method, null: false #qpay, candy, social pay etc
      t.string :qinvoice, null: true, limit: 20
      t.string :customer, null: true, limit: 10 #for candy
      t.boolean :is_paid, null: false, :default => false
      t.boolean :is_added, null: false, :default => false
      t.timestamps
    end
  end
end
