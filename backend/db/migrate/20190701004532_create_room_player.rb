class CreateRoomPlayer < ActiveRecord::Migration[5.2]
  def change
    create_table :room_players do |t|
      t.references :room, index: true, foreign_key: true
      t.references :player, index: true, foreign_key: true
      t.integer :match_cnt, null: false, :default => 0
      t.string :last_match, :limit => 100, null: false
      t.integer :place, null: false, :default => 0
      t.integer :status, null: false, :default => 0
      t.datetime :end_date, null: true
      t.timestamps
    end
  end
end
