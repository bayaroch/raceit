class CreateDeviseToPlayers < ActiveRecord::Migration[5.2]
  def self.up
    create_table :players do |t|
      ## Database authenticatable
      t.string :uid, :limit => 100, null: false, :unique => true  
      t.string :name, :limit => 100, null: false, :unique => true
      t.string :nickname, :limit => 100, null: false
      t.string :email, :limit => 50, null: true, default: ""
      t.string :account, :limit => 100, null: true
      t.string :location, :limit => 5, null: true
      t.string :image, null: true
      t.integer :status, null: false, :default => 0
      
      t.string :encrypted_password, null: false, default: ""

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      
      t.timestamps
    end

    add_index :players, :uid,                unique: true
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
