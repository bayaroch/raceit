class CreateTransaction < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.references :player, index: true, null: false, foreign_key: true
      t.references :room, index: true, null: true, foreign_key: true
      t.decimal :bfr, null: false, :default => 0
      t.decimal :amount, null: false, :default => 0
      t.decimal :aftr, null: false, :default => 0
      t.integer :type, null: false, :default => 0
    end
  end
end
