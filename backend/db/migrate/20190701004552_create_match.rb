class CreateMatch < ActiveRecord::Migration[5.2]
  def change
    create_table :matches do |t|
      t.references :room, index: true, foreign_key: true
      t.references :player, index: true, foreign_key: true
      t.string :match, :limit => 100, null: false
      t.datetime :match_date, null: false
      t.integer :damage_dealt, null: false, :default => 0
      t.integer :kills, null: false, :default => 0
      t.integer :win_place, null: false, :default => 0
      t.timestamps
    end
  end
end
