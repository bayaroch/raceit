# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_20_064038) do

  create_table "admins", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "provider", limit: 50, null: false
    t.string "uid", limit: 50, null: false
    t.string "name", limit: 50, null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "role", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
  end

  create_table "invoices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "player_id"
    t.decimal "amount", precision: 10, null: false
    t.integer "method", null: false
    t.string "qinvoice", limit: 20
    t.string "customer", limit: 10
    t.boolean "is_paid", default: false, null: false
    t.boolean "is_added", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["player_id"], name: "index_invoices_on_player_id"
  end

  create_table "jobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matches", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "room_id"
    t.bigint "player_id"
    t.string "match", limit: 100, null: false
    t.datetime "match_date", null: false
    t.integer "damage_dealt", default: 0, null: false
    t.integer "kills", default: 0, null: false
    t.integer "win_place", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["player_id"], name: "index_matches_on_player_id"
    t.index ["room_id"], name: "index_matches_on_room_id"
  end

  create_table "players", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "uid", limit: 100, null: false
    t.string "name", limit: 100, null: false
    t.string "nickname", limit: 100, null: false
    t.string "email", limit: 50, default: ""
    t.string "account", limit: 100
    t.string "location", limit: 5
    t.string "image"
    t.integer "status", default: 0, null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_players_on_uid", unique: true
  end

  create_table "ppoints", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "point", default: 0, null: false
  end

  create_table "room_players", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "room_id"
    t.bigint "player_id"
    t.integer "match_cnt", default: 0, null: false
    t.string "last_match", limit: 100, null: false
    t.integer "place", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["player_id"], name: "index_room_players_on_player_id"
    t.index ["room_id"], name: "index_room_players_on_room_id"
  end

  create_table "rooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.integer "full_cnt", default: 50, null: false
    t.integer "member_cnt", default: 0, null: false
    t.integer "match_cnt", default: 3, null: false
    t.integer "status", default: 0, null: false
    t.integer "method", default: 0, null: false
    t.integer "place_cnt", default: 10, null: false
    t.integer "ticket", default: 1000, null: false
    t.decimal "amount", precision: 10, default: "0", null: false
    t.integer "time_limit", default: 120, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "player_id", null: false
    t.bigint "room_id"
    t.decimal "bfr", precision: 10, default: "0", null: false
    t.decimal "amount", precision: 10, default: "0", null: false
    t.decimal "aftr", precision: 10, default: "0", null: false
    t.integer "type", default: 0, null: false
    t.index ["player_id"], name: "index_transactions_on_player_id"
    t.index ["room_id"], name: "index_transactions_on_room_id"
  end

  add_foreign_key "invoices", "players"
  add_foreign_key "matches", "players"
  add_foreign_key "matches", "rooms"
  add_foreign_key "room_players", "players"
  add_foreign_key "room_players", "rooms"
  add_foreign_key "transactions", "players"
  add_foreign_key "transactions", "rooms"
end
